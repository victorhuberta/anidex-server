var cheerio = require('cheerio');
var request = require('request');
var streamToArray = require('stream-to-array');

var Clarifai = require('clarifai');
var clarifai = new Clarifai({
 id: 'kkz_Hxnp8bS5JctolcJnd3O3PYEFH9lo2wfTPCPd',
 secret: 'ZZH7QcRcRrqxChLQuSMO8oHnpFwAWtyruHhPaYAb'
});

var animalProfileLocations = {};

module.exports = new AnimalsController();

gatherAnimalUrls();

// var fs = require('fs');
// fs.readFile('/home/victorhuberta/Desktop/PICTURES/animals/animal4.jpg', function (err, buffer) {
//   clarifai.tagFromBuffers('image', buffer, function (err, results) {
//     console.log(results);
//   });
// });

function AnimalsController() {
  this.getInfo = function (req, res, next) {
    console.log('Getting animal information...');
    if (Object.keys(animalProfileLocations).length === 0) {
      gatherAnimalUrls();
    }

    req.busboy.on('file', function(fieldname, file, filename, encoding, mimetype) {
      streamToArray(file)
        .then(function (parts) {
            var buffers = [];
            for (var i = 0, l = parts.length; i < l ; ++i) {
              var part = parts[i];
              buffers.push((part instanceof Buffer) ? part : new Buffer(part));
            }
            var imageBuffer = Buffer.concat(buffers);
            console.log('Contacting Clarifai...');
            clarifai.tagFromBuffers('image', imageBuffer, function (err, results) {
              console.log(results);
              for (var tag of results.tags) {
                console.log(tag);
                if (animalProfileLocations[tag['class'].toLowerCase()] !== undefined) {
                  console.log(tag['class']);
                  return getAnimalProfile(tag['class'], res);
                }
              }
              return res.json({});
            });
          });
    });
    // req.busboy.on('field', function(key, value, keyTruncated, valueTruncated) {
      
    // });
  };
}

function gatherAnimalUrls() {
  var url = 'http://a-z-animals.com/animals/';
  request(url, function (error, response, html) {
    if (!error) {
      console.log('Gathering animal URLs...');
      var $ = cheerio.load(html);
      $('.article_az').filter(function () {
        $(this).children('tr').each(function (i, elem) {
          $(this).children('td').each(function (i, elem) {
            $(this).children('ul').each(function (i, elem) {
              $(this).children('li').each(function (i, elem) {
                var animalName = $(this).children('a').text();
                var profileUrl = $(this).children('a').attr('href');
                animalProfileLocations[animalName.toLowerCase()] = profileUrl;
              });
            });
          });
        });
      });
    } else {
      console.log(error);
    }
  });
}

function getAnimalProfile(name, res) {
  console.log('Getting animal profile...');
  var profileUrl = animalProfileLocations[name];
  var fullUrl = 'http://a-z-animals.com' + profileUrl;
  request(fullUrl, function (error, response, html) {
    if (!error) {
      var $ = cheerio.load(html);
      var profile = {};
      var tagCount = 0;

      $('.article_facts').filter(function () {
        var allTrTags = $(this).children('tr');
        allTrTags.each(function (i, elem) {
          tagCount += 1;
          var data = $(this);
          var b = data.children().first().children().first();

          var key = b.children().first().text() === '' ? b.text() : b.children().first().text();
          var value = data.children('td').last().text();
          if (key !== '' && value !== '') {
            profile[key] = value; 
          }

          if (tagCount >= allTrTags.length) {
            var animalInfo = {
              name, profile
            };

            return res.json(animalInfo);
          }

        });
      });
    } else {
      return res.json({ 'error': error.message });
    }
  });
}