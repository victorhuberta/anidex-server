var express = require('express');
var router = express.Router();
var animals = require('../app/controllers/animals.server.controller');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Anidex' });
});

router.post('/api/animals/info', animals.getInfo);

module.exports = router;
